package com.example.zzeulki.myapplication2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class Act2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act2);
    }

    public void Onclick(View v)
    {
        switch(v.getId()) {

            case  R.id.act2_go1 : {
                Log.i("act2.act2_go1", "onClick");
                Intent intent1 = new Intent(Act2.this, Act1.class);
                startActivity(intent1);
                break;
            }

            case  R.id.act2_go3 : {
                Log.i("act2.act2_go3", "onClick");
                Intent intent1 = new Intent(Act2.this, Act3.class);
                startActivity(intent1);
                break;
            }
        }
    }
}
