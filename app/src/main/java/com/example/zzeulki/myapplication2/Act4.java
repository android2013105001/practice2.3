package com.example.zzeulki.myapplication2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class Act4 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act4);
    }

    public void Onclick(View v)
    {
        switch(v.getId()) {

            case  R.id.act4_go3 : {
                Log.i("act4.act4_go3", "onClick");
                Intent intent1 = new Intent(Act4.this, Act3.class);
                startActivity(intent1);
                break;
            }
        }
    }
}
