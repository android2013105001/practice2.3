package com.example.zzeulki.myapplication2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class Act3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act3);
    }


    public void Onclick(View v) {
        switch (v.getId()) {

            case R.id.act3_go2: {
                Log.i("act3.act3_go2", "onClick");
                Intent intent1 = new Intent(Act3.this, Act2.class);
                startActivity(intent1);
                break;
            }

            case R.id.act3_go4: {
                Log.i("act3.act3_go4", "onClick");
                Intent intent1 = new Intent(Act3.this, Act4.class);
                startActivity(intent1);
                break;
            }
        }
    }
}